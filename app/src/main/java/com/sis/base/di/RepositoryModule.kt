package com.sis.base.di

import com.sis.base.data.repository.FavoriteItemRepositoryImpl
import com.sis.base.data.repository.ItemRepositoryImpl
import com.sis.base.domain.repository.FavoriteItemRepository
import com.sis.base.domain.repository.ItemsRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

/**
 * RepositoryModule is to bind di for all repositories
 *
 * @author mojtaba hemmati sis
 */
@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun bindFavItemsRepository(nameRepositoryImpl: FavoriteItemRepositoryImpl): FavoriteItemRepository
    @Binds
    abstract fun bindItemsRepository(nameRepositoryImpl: ItemRepositoryImpl): ItemsRepository

}