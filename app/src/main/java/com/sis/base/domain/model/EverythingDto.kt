package com.sis.base.domain.model

import androidx.room.PrimaryKey

data class EverythingDto (
    val author: String,
    val title: String,
    val description: String,
    val url: String,
    val urlToImage: String,
    val publishedAt: String
)