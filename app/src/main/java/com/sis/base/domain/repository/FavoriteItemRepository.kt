package com.sis.base.domain.repository

import androidx.paging.PagingData
import com.sis.base.data.db.fav.FavoriteData
import com.sis.base.domain.model.ItemDto
import kotlinx.coroutines.flow.Flow

interface FavoriteItemRepository {

    suspend fun getAll(): Flow<PagingData<ItemDto>>
    suspend fun removeFavorite(data: FavoriteData): Boolean



}