package com.sis.base.domain.repository

import androidx.paging.PagingData
import com.sis.base.data.db.fav.FavoriteData
import com.sis.base.domain.model.ItemDto
import kotlinx.coroutines.flow.Flow

interface ItemsRepository {

    fun getLastSearchText(): String?
    suspend fun addSearchText(searchText: String?)
    suspend fun getItemsWithFavorite(): Flow<PagingData<ItemDto>>
    suspend fun changeFavoriteItem(favData: FavoriteData, isFavLastState: Boolean)

}