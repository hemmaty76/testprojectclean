package com.sis.base.domain.model

data class ItemDto(
    val author: String,
    val title: String,
    val description: String,
    val url: String,
    val urlToImage: String,
    val publishedAt: String,
    var isFave: Boolean
)
