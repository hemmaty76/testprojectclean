package com.sis.base.data.db.item

import androidx.room.Embedded
import com.sis.base.data.db.item.everything.EverythingData

data class ItemData(
    @Embedded val postData: EverythingData,
    val isFavorite: Boolean
)