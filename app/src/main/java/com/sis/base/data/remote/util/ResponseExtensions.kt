package com.sis.base.data.remote.util

import retrofit2.Response

suspend fun <T> Response<BaseCallback<T>>.handleResponse(): Resource<T> {

    when (val apiResponse = ApiResponse.create(this)) {
        is ApiSuccessResponse -> {
            return if (apiResponse.body.status == "ok")
                Resource.success(apiResponse.body.data)
            else {
                Resource.error(
                    apiResponse.body.message,
                    apiResponse.body.data,
                    ApiError.ERROR_CODE_0
                )
            }
        }

        is ApiErrorResponse -> {
            return Resource.error(
                apiResponse.errorMessage,
                null,
                apiResponse.error
            )
        }
    }
}
