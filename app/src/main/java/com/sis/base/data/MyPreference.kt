package com.sis.base.data

import android.content.Context
import androidx.preference.PreferenceManager
import com.google.gson.Gson
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MyPreference @Inject constructor(@ApplicationContext context: Context) {
    private val SEARCH_TEXT: String = "searchText"
    val prefs = PreferenceManager.getDefaultSharedPreferences(context)
    val gson = Gson()

    fun getSearchText(): String? {
        prefs.getString(SEARCH_TEXT, null)?.let {
            return it
        }
        return null
    }

    fun saveSearchText(searchText: String?) {
        prefs.edit().putString(SEARCH_TEXT, searchText).apply()
    }


}