package com.sis.base.data.remote.util

import com.google.gson.Gson
import com.sis.base.data.remote.util.ApiError
import com.sis.base.data.remote.util.BaseCallback
import com.sis.base.data.remote.util.getApiError
import retrofit2.Response


@Suppress("unused")
sealed class ApiResponse<T> {
    companion object {
        val gson = Gson()

        fun <T> create(error: Throwable): ApiErrorResponse<T> {
            return ApiErrorResponse(null, error.getApiError())
        }

        fun <T> create(response: Response<T>): ApiResponse<T> {
            return if (response.isSuccessful) {
                val body = response.body()
                if (body != null && response.code() == 200)
                    ApiSuccessResponse(body)
                else
                    ApiErrorResponse(null, response.getApiError())
            } else {
                var msg: String? = null
                try {
                    val errorBody = gson.fromJson(response.errorBody()?.string(), BaseCallback::class.java)
                    msg = errorBody?.message
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                ApiErrorResponse(msg, response.getApiError())
            }
        }
    }
}


data class ApiSuccessResponse<T>(val body: T) : ApiResponse<T>()
data class ApiErrorResponse<T>(val errorMessage: String?, val error: ApiError) : ApiResponse<T>()
