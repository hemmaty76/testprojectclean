package com.sis.base.data.remote.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class SourceModel(
    @SerializedName("id") val id: String?,
    @SerializedName("name") val name: String?
)