package com.sis.base.data.repository

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.sis.base.data.MyPreference
import com.sis.base.data.db.fav.FavoriteDao
import com.sis.base.data.db.fav.FavoriteData
import com.sis.base.data.db.item.ItemDao
import com.sis.base.data.db.item.everything.EverythingDao
import com.sis.base.data.db.item.remoteKey.ItemRemoteKeysDao
import com.sis.base.data.mapper.toDto
import com.sis.base.data.remote.ApiService
import com.sis.base.data.repository.source.ItemsRemoteMediator
import com.sis.base.domain.model.ItemDto
import com.sis.base.domain.repository.ItemsRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class ItemRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
    private val everythingDao: EverythingDao,
    private val itemDao: ItemDao,
    private val favoriteDao: FavoriteDao,
    private val remoteKeysDao: ItemRemoteKeysDao,
    private val myPreference: MyPreference
) : ItemsRepository {

    override fun getLastSearchText(): String? {
        return myPreference.getSearchText()
    }

    override suspend fun addSearchText(searchText: String?) {
        myPreference.saveSearchText(searchText)
    }

    @OptIn(ExperimentalPagingApi::class)
    override suspend fun getItemsWithFavorite(): Flow<PagingData<ItemDto>> {

        return Pager(
            config = PagingConfig(
                pageSize = 10,
                enablePlaceholders = true
            ),
            pagingSourceFactory = {
                itemDao.getData()
            },
            remoteMediator = ItemsRemoteMediator(apiService, everythingDao, remoteKeysDao,myPreference)
        ).flow.map { pagingData ->
            pagingData.map {
                it.toDto()
            }
        }
    }

    override suspend fun changeFavoriteItem(favData: FavoriteData, isFavLastState: Boolean) {
        if (isFavLastState) {
            favoriteDao.delete(favData)
        } else {
            favoriteDao.insert(favData)
        }
        delay(100)
    }


}