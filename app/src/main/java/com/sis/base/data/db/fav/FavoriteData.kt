package com.sis.base.data.db.fav

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class FavoriteData (
    val author: String,
    val title: String,
    val description: String,
    @PrimaryKey
    val url: String,
    val urlToImage: String,
    val publishedAt: String
)