package com.sis.base.data.remote.util

import android.util.Log
import retrofit2.CallAdapter.Factory
import retrofit2.CallAdapter
import retrofit2.Response
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

class CallAdapterFactory : Factory() {
    override fun get(
        returnType: Type,
        annotations: Array<out Annotation>,
        retrofit: Retrofit
    ): CallAdapter<*, *>? {

        Log.i("TAGabs", "get: ${getRawType(returnType)}" )
        if (getRawType(returnType) == Response::class.java) {

            val observableType = getParameterUpperBound(0, returnType as ParameterizedType)
            val rawObservableType = getRawType(observableType)

            if (rawObservableType != BaseCallback::class.java) {
                throw IllegalArgumentException("type must be a resource")
            }

            if (observableType !is ParameterizedType) {
                throw IllegalArgumentException("resource must be parameterized")
            }
            return DefaultCallAdapter<Any>(observableType)
        }else return null
    }


}
