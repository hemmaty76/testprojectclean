package com.sis.base.data.remote.util


sealed class RemoteException() : Exception() {
    class HttpResponseException(var resource: Resource<*>) : RemoteException()
    
}