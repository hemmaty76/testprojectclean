package com.sis.base.data.remote.util

import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Response
import java.lang.reflect.Type

class DefaultCallAdapter<R>(private val responseType: Type) : CallAdapter<R, Response<R>> {

    override fun responseType(): Type = responseType

    override fun adapt(call: Call<R>): Response<R> {
        return call.execute()
    }

}