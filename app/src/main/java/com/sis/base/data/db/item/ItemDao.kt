package com.sis.base.data.db.item

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface ItemDao {

    @Query("SELECT E.*, (SELECT CASE WHEN EXISTS (SELECT * FROM FavoriteData F WHERE F.url = E.url) THEN 1 ELSE 0 END) AS isFavorite FROM EverythingData E")
    fun getData(): PagingSource<Int, ItemData>

//    @Query("SELECT E.*, COUNT(F.url) as isFavorite FROM EverythingData E LEFT JOIN FavoriteData F ON E.url = F.url GROUP BY E.url")
//    fun getData2(): PagingSource<Int, ItemData>

}