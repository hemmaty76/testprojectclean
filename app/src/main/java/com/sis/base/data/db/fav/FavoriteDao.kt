package com.sis.base.data.db.fav

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.sis.base.data.db.item.ItemData

@Dao
interface FavoriteDao {

    @Query("SELECT * FROM FavoriteData")
    fun getData(): PagingSource<Int, FavoriteData>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(item: FavoriteData)

    @Delete
    suspend fun delete(item: FavoriteData)
}