package com.sis.base.data.remote.util

import com.google.gson.annotations.SerializedName

open class BaseCallback<T>(
    @SerializedName("articles")
    val data: T?,
    @SerializedName("totalResults")
    val totalResults: Int?,
    @SerializedName("status")
    val status: String,
    @SerializedName("message")
    val message: String?
)