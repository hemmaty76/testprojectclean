package com.sis.base.data.repository.source

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import com.sis.base.data.MyPreference
import com.sis.base.data.db.item.ItemData
import com.sis.base.data.db.item.everything.EverythingDao
import com.sis.base.data.db.item.remoteKey.ItemRemoteKeys
import com.sis.base.data.db.item.remoteKey.ItemRemoteKeysDao
import com.sis.base.data.mapper.toDbModelList
import com.sis.base.data.remote.ApiService
import com.sis.base.data.remote.util.Status
import com.sis.base.data.remote.util.getStringMessage
import com.sis.base.data.remote.util.handleResponse
import com.sis.base.utils.API_KEY
import com.sis.base.utils.API_VERSION
import com.sis.base.utils.NoActionException
import com.sis.base.utils.PAGE_SIZE
import java.util.concurrent.TimeUnit

@OptIn(ExperimentalPagingApi::class)
class ItemsRemoteMediator(
    private val apiService: ApiService,
    private val itemDao: EverythingDao,
    private val remoteKeysDao: ItemRemoteKeysDao,
    private val myPreference: MyPreference,
) : RemoteMediator<Int, ItemData>() {

    override suspend fun initialize(): InitializeAction {
        val cacheTimeout = TimeUnit.MILLISECONDS.convert(1, TimeUnit.HOURS)

        return if (System.currentTimeMillis() - (remoteKeysDao.getCreationTime()
                ?: 0) < cacheTimeout
        ) {
            InitializeAction.SKIP_INITIAL_REFRESH
        } else {
            InitializeAction.LAUNCH_INITIAL_REFRESH
        }
    }

    private suspend fun getRemoteKeyForLastItem(state: PagingState<Int, ItemData>): ItemRemoteKeys? {

        return state.pages.lastOrNull {
            it.data.isNotEmpty()
        }?.data?.lastOrNull()?.let { item ->
            remoteKeysDao.getRemoteKeyByItemID(item.postData.url)
        }
    }

    private suspend fun getRemoteKeyForFirstItem(state: PagingState<Int, ItemData>): ItemRemoteKeys? {
        return state.pages.firstOrNull {
            it.data.isNotEmpty()
        }?.data?.firstOrNull()?.let { item ->
            remoteKeysDao.getRemoteKeyByItemID(item.postData.url)
        }
    }

    private suspend fun getRemoteKeyClosestToCurrentPosition(state: PagingState<Int, ItemData>): ItemRemoteKeys? {
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.postData?.url?.let { id ->
                remoteKeysDao.getRemoteKeyByItemID(id)
            }
        }
    }

    var searchText : String? = myPreference.getSearchText()
    override suspend fun load(
        loadType: LoadType, state: PagingState<Int, ItemData>): MediatorResult {

        val page: Int = when (loadType) {
            LoadType.REFRESH -> {
                searchText = myPreference.getSearchText()
                remoteKeysDao.clearRemoteKeys()
                1
            }

            LoadType.PREPEND -> {
                val remoteKeys = getRemoteKeyForFirstItem(state)
                val prevKey = remoteKeys?.prevKey
                prevKey
                    ?: return MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
            }

            LoadType.APPEND -> {
                val remoteKeys = getRemoteKeyForLastItem(state)
                val nextKey = remoteKeys?.nextKey
                nextKey ?: return MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
            }
        }
        try {

            if (searchText.isNullOrBlank()){
                return MediatorResult.Error(NoActionException("Please Add Search Text"))
            }

            val apiResponse = apiService.getItems(API_VERSION,API_KEY,searchText!!,page,PAGE_SIZE).handleResponse()

            when (apiResponse.status) {
                Status.ERROR -> {
                    return MediatorResult.Error(Throwable(apiResponse.error?.getStringMessage()?:apiResponse.message))
                }

                Status.SUCCESS -> {
                    apiResponse.data?.let {
                        val everythingItems = it
                        val endOfPaginationReached = everythingItems.isEmpty()

                        if (loadType == LoadType.REFRESH) {
                            itemDao.clearAllItems()
                        }
                        val prevKey = if (page > 1) page - 1 else null
                        val nextKey = if (endOfPaginationReached) null else page + 1
                        val remoteKeys = everythingItems.map {
                            ItemRemoteKeys(
                                it.url,
                                prevKey = prevKey,
                                currentPage = page,
                                nextKey = nextKey
                            )
                        }
                        remoteKeysDao.insertAll(remoteKeys)
                        itemDao.insertAll(everythingItems.toDbModelList())
                        return MediatorResult.Success(endOfPaginationReached = endOfPaginationReached)

                    }
                }
            }

        } catch (error: Exception) {
            return MediatorResult.Error(error)
        }

        return MediatorResult.Error(Throwable("Unknow Error"))

    }

}