package com.sis.base.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.sis.base.data.db.fav.FavoriteDao
import com.sis.base.data.db.fav.FavoriteData
import com.sis.base.data.db.item.ItemDao
import com.sis.base.data.db.item.everything.EverythingDao
import com.sis.base.data.db.item.everything.EverythingData
import com.sis.base.data.db.item.remoteKey.ItemRemoteKeys
import com.sis.base.data.db.item.remoteKey.ItemRemoteKeysDao


@Database(entities = [EverythingData::class,FavoriteData::class,ItemRemoteKeys::class], version = 1, exportSchema = false)
abstract class DataBase : RoomDatabase() {

    abstract fun everythingDao(): EverythingDao
    abstract fun favDao(): FavoriteDao
    abstract fun itemDao(): ItemDao
    abstract fun itemRemoteKeyDao(): ItemRemoteKeysDao


}