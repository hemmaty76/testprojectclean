package com.sis.base.data.db.item.everything

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

@Dao
interface EverythingDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(items: List<EverythingData>)

    @Query("UPDATE EverythingData SET title=:title WHERE url=:url")
    suspend fun update(url :String,title:String)

    @Query("Select * From everythingData")
    fun getItems(): PagingSource<Int, EverythingData>

    @Query("Delete From everythingData")
    suspend fun clearAllItems()

}