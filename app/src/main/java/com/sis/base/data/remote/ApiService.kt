package com.sis.base.data.remote

import com.sis.base.data.remote.model.EverythingModel
import com.sis.base.data.remote.util.BaseCallback
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("v{version}/everything")
    suspend fun getItems(
        @Path("version") version: String,
        @Query("apiKey") apiKey: String,
        @Query("q") searchText: String,
        @Query("page") page: Int,
        @Query("pageSize") pageSize: Int
    ) : Response<BaseCallback<ArrayList<EverythingModel>>>

}