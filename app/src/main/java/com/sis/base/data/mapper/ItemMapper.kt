package com.sis.base.data.mapper

import com.sis.base.data.db.fav.FavoriteData
import com.sis.base.data.db.item.ItemData
import com.sis.base.data.db.item.everything.EverythingData
import com.sis.base.data.remote.model.EverythingModel
import com.sis.base.domain.model.ItemDto


fun ItemData.toDto() = ItemDto(
    author = postData.author,
    title = postData.title,
    description = postData.description?:"",
    url = postData.url,
    urlToImage = postData.urlToImage,
    publishedAt = postData.publishedAt,
    isFave = isFavorite
)

fun FavoriteData.toDto() = ItemDto(
    author = author,
    title = title,
    description = description,
    url =url,
    urlToImage = urlToImage,
    publishedAt = publishedAt,
    isFave = true
)

fun ItemDto.toFavoriteData() = FavoriteData(
    author = author,
    title = title,
    description = description,
    url = url,
    urlToImage = urlToImage,
    publishedAt = publishedAt
)

fun FavoriteData.toEverythingData() = EverythingData(
    author = author,
    title = title,
    description = description,
    url = url,
    urlToImage = urlToImage,
    publishedAt = publishedAt,
)

fun EverythingModel.toDbModel() = EverythingData(
    author = author?:"",
    title = title,
    description = description,
    url = url,
    urlToImage = urlToImage?:"",
    publishedAt = publishedAt,
)


fun List<ItemData>.toItemDtoList(): List<ItemDto> = this.map { it.toDto() }

fun List<EverythingModel>.toDbModelList(): List<EverythingData> = this.map { it.toDbModel() }



