package com.sis.base.data.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.sis.base.data.MyPreference
import com.sis.base.data.db.fav.FavoriteDao
import com.sis.base.data.db.fav.FavoriteData
import com.sis.base.data.db.item.everything.EverythingDao
import com.sis.base.data.mapper.toDto
import com.sis.base.domain.model.ItemDto
import com.sis.base.domain.repository.FavoriteItemRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class FavoriteItemRepositoryImpl @Inject constructor(
    private val favDao: FavoriteDao,
) : FavoriteItemRepository {

    override suspend fun getAll(): Flow<PagingData<ItemDto>> {
        return Pager(
            PagingConfig(
                pageSize = 10,
                enablePlaceholders = false
            ),
        ) {
            favDao.getData()
        }.flow.map { pagingData ->
            pagingData.map {
                it.toDto()
            }
        }
    }

    override suspend fun removeFavorite(item: FavoriteData): Boolean {
        favDao.delete(item)
        return false
    }


}