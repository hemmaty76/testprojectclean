package com.sis.base.presention.main.home.favorite.viewmodel

import com.sis.base.domain.model.ItemDto

sealed class FavoriteItemState {
    object Idle : FavoriteItemState()
    data class RemoveFav(val item:ItemDto) : FavoriteItemState()
}
