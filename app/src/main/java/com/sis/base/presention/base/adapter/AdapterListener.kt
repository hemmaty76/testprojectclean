package com.sis.base.presention.base.adapter

interface AdapterListener {
    fun onLoadMore()
    fun changeState(state: AdapterState,count:Int)
}