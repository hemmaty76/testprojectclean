package com.sis.base.presention.main.home.items.viewModel

import com.sis.base.data.db.fav.FavoriteData
import com.sis.base.domain.model.ItemDto
import com.sis.base.presention.base.BaseIntent

sealed class ItemListIntent :BaseIntent(){
    object GetItemList : ItemListIntent()
    data class AddSearchText(val searchText:String?): ItemListIntent()
    data class ChangeFav(val position: Int,val itemDto: ItemDto): ItemListIntent()
}
