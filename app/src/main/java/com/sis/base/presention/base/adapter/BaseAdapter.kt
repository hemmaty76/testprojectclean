package com.sis.base.presention.base.adapter

import android.annotation.SuppressLint
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<T> : RecyclerView.Adapter<BaseViewHolder<out ViewDataBinding, T>?>() {
    val DATA_TYPE = 1
    lateinit var recyclerView: RecyclerView
    var dataList: ArrayList<T?>? = null
    var isLoading = false
        set(value) {
            field = value
            sendState()
        }

    private fun sendState() {
        if (isLoading)
            stateListener?.changeState(AdapterState.Loading, getItemCount(false))
        else if (dataList.isNullOrEmpty())
            stateListener?.changeState(AdapterState.Empty, getItemCount(false))
        else
            stateListener?.changeState(AdapterState.Nothing, getItemCount(false))
    }

    var hasMoreDataAvailable = false
    var autoLoadMore = true
    open var stateListener: AdapterListener? = null
    val baseAdapter: BaseAdapter<*>
        get() = this
    val baseAdapterByModel: BaseAdapter<T>
        get() = this

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun getItemViewType(position: Int): Int {
        return if (dataList == null || dataList!![position] == null) -1 else DATA_TYPE
    }

    override fun getItemCount(): Int {
        return getItemCount(true)
    }

    private fun getItemCount(sendState: Boolean): Int {
        if (dataList == null) {
            return 0
        }
        if (sendState)
            sendState()
        return dataList!!.size
    }

    fun getDataList(): List<T?>? {
        return dataList
    }

    fun setMoreDataAvailable(isMoreDataAvailable: Boolean) {
        this.hasMoreDataAvailable = isMoreDataAvailable
    }

    fun addLoadingItem() {
        if (dataList != null) {
            dataList!!.add(null)
            recyclerView.post {
                notifyItemInserted(dataList!!.size - 1)
            }
        }
    }

    fun removeLoadingItem() {
        if (dataList != null) {
            val size = dataList!!.size
            if (size > 0 && dataList!![size - 1] == null) {
                removeItemByPosition(size - 1)
            }
        }
    }


    fun removeItemByPosition(pos: Int) {
        if (dataList != null && dataList!!.size > pos) {
            dataList!!.removeAt(pos)

            notifyItemRemoved(pos)
            notifyItemRangeChanged(pos, itemCount - pos)
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun notifyDataChanged() {

        notifyDataSetChanged()
        isLoading = false

    }

    fun addItemsToList(list: List<T?>?) {
        if (list != null) {
            if (dataList == null) {
                setList(list)
            } else if (dataList!!.isEmpty()) {
                dataList!!.addAll(list)
                notifyDataChanged()
            } else {
                isLoading = false
                val size = dataList!!.size
                dataList!!.addAll(list)

                notifyItemRangeInserted(size, list.size)

            }
        }
    }

    fun addItemToList(t: T) {
        if (dataList == null) {
            setSingleList(t)
        } else if (dataList!!.isEmpty()) {
            dataList!!.add(t)
            notifyDataChanged()
        } else {
            isLoading = false
            val size = dataList!!.size
            dataList!!.add(t)
            notifyItemInserted(size)
        }
    }

    fun addToPosList(pos: Int, t: T) {
        if (dataList == null) {
            setSingleList(t)
        } else if (dataList!!.isEmpty()) {
            dataList!!.add(t)
            notifyDataChanged()
        } else {
            isLoading = false
            dataList!!.add(pos, t)
            notifyItemInserted(pos)
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setList(list: List<T?>) {
        dataList = ArrayList(list)
        notifyDataChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setSingleList(t: T) {
        dataList = ArrayList()
        dataList!!.add(t)
        notifyDataChanged()
    }

    fun clear(refresh: Boolean) {
        if (dataList != null) {
            dataList!!.clear()
            dataList = null
        }
        if (refresh) notifyDataChanged()
    }

    val isEmpty: Boolean
        get() = dataList == null || dataList!!.isEmpty()

    fun itemChang(data: T, position: Int) {
        if (dataList != null) {
            dataList!!.removeAt(position)
            dataList!!.add(position, data)
            notifyItemChanged(position)
        }
    }
}