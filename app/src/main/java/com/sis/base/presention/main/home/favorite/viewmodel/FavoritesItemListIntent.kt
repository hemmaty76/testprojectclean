package com.sis.base.presention.main.home.favorite.viewmodel

import com.sis.base.domain.model.ItemDto
import com.sis.base.presention.base.BaseIntent

sealed class FavoritesItemListIntent : BaseIntent() {
    object GetItemList : FavoritesItemListIntent()
    data class RemoveFavorite(val item:ItemDto) : FavoritesItemListIntent()
}
