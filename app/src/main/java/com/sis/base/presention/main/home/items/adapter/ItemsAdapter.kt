package com.sis.base.presention.main.home.items.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagingDataAdapter
import com.sis.base.R
import com.sis.base.databinding.AdapterItemLayoutBinding
import com.sis.base.domain.model.ItemDto
import com.sis.base.presention.base.adapter.BaseViewHolder


class ItemsAdapter(var listener: Listener) :
    PagingDataAdapter<ItemDto, ItemsAdapter.ViewHolder>(ItemsDiffCallBack()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(getItem(position), position)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.adapter_item_layout,
                parent,
                false
            )
        )
    }

    fun customNotifyItemChanged(itemDto: ItemDto, position: Int) {
//        Log.i("TAGabs", " ------------------------------------- ")
//        Log.i("TAGabs", "placeholdersAfter: " + snapshot().placeholdersAfter)
//        Log.i("TAGabs", "placeholdersBefore: " + snapshot().placeholdersBefore)
//        Log.i("TAGabs", "snapshot().size: " + snapshot().size)
//        Log.i("TAGabs", "position: " + position)
//        Log.i("TAGabs", "itemCount: " + itemCount)
//        Log.i("TAGabs", "itemDto: " + itemDto.url)
//        Log.i("TAGabs", "itemlst: " + snapshot().items[position-snapshot().placeholdersBefore].url)

        notifyItemChanged(position)
    }

    inner class ViewHolder(binding: AdapterItemLayoutBinding) : BaseViewHolder<AdapterItemLayoutBinding, ItemDto>(binding) {

        fun onBind(item: ItemDto?, position: Int) {
            item?.let {
                binding.item = item

                binding.like.setOnClickListener {
                    listener.requestChangeFavorite(position, item)
//                    item.isFave = !item.isFave
//                    binding.like.likeState(item.isFave)
                }
            }
        }

    }

    interface Listener {
        fun requestChangeFavorite(position: Int, itemDto: ItemDto)
    }


}