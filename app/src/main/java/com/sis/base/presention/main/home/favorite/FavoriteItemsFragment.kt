package com.sis.base.presention.main.home.favorite

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.SimpleItemAnimator
import com.sis.base.R
import com.sis.base.databinding.FragmentFavItemsBinding
import com.sis.base.domain.model.ItemDto
import com.sis.base.presention.base.BaseFragment
import com.sis.base.presention.main.dialog.DialogBottomSheet
import com.sis.base.presention.main.home.favorite.viewmodel.FavoritesItemListIntent
import com.sis.base.presention.main.home.favorite.viewmodel.FavoriteItemState
import com.sis.base.presention.main.home.favorite.viewmodel.FavoritesItemViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FavoriteItemsFragment : BaseFragment<FragmentFavItemsBinding>() {

    private val viewModel: FavoritesItemViewModel by viewModels()

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentFavItemsBinding
        get() = FragmentFavItemsBinding::inflate

    override fun setupUi() {
        with(binding) {
            vm = viewModel
            lifecycleOwner = this@FavoriteItemsFragment
            (recyclerView.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        }
        viewModel.dataState.observe(viewLifecycleOwner) {
            when (it) {
                is FavoriteItemState.RemoveFav -> {
                    requestRemoveItemFromFavorite(it.item)
                }

                is FavoriteItemState.Idle -> {}
            }
        }
    }

    private fun requestRemoveItemFromFavorite(item: ItemDto) {
        DialogBottomSheet.getInstance(getString(R.string.wrong), getString(R.string.sureRemoveFromFavorite,item.title))
            .setNegativeListener(getString(R.string.yes)){
                viewModel.onTriggerEvent(FavoritesItemListIntent.RemoveFavorite(item))
            }
            .setPositiveListener(getString(R.string.no))
            .show(childFragmentManager, "")

    }

    override fun setupData() {
        viewModel.onTriggerEvent(FavoritesItemListIntent.GetItemList)
    }

    override fun isFetchData(): Boolean {
        return viewModel.isFetchData()
    }

    override fun onStop() {
        viewModel.dataState.postValue(FavoriteItemState.Idle)
        super.onStop()
    }


}