package com.sis.base.presention.main.home.items

import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.SimpleItemAnimator
import com.sis.base.databinding.FragmentItemsBinding
import com.sis.base.presention.base.BaseFragment
import com.sis.base.presention.main.home.items.viewModel.ItemListIntent
import com.sis.base.presention.main.home.items.viewModel.ItemsViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class ItemsFragment : BaseFragment<FragmentItemsBinding>() {

    private val viewModel: ItemsViewModel by viewModels()

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentItemsBinding
        get() = FragmentItemsBinding::inflate

    override fun setupUi() {
        with(binding) {
            vm = viewModel
            lifecycleOwner = this@ItemsFragment
            (recyclerView.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false

            toolbarSearch?.searchText?.apply {
                setText(viewModel.getLastSearchText())
                setOnEditorActionListener { _, actionId, _ ->
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        viewModel.onTriggerEvent(ItemListIntent.AddSearchText(text.toString()))
                    }
                    true
                }

            }


        }
    }

    override fun setupData() {
        viewModel.onTriggerEvent(ItemListIntent.GetItemList)
    }

    override fun isFetchData(): Boolean {
        return viewModel.isFetchData()
    }

}