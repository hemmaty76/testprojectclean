package com.sis.base.presention.main.home.favorite.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.sis.base.data.mapper.toFavoriteData
import com.sis.base.domain.model.ItemDto
import com.sis.base.domain.repository.FavoriteItemRepository
import com.sis.base.presention.base.BaseViewModel
import com.sis.base.presention.main.home.items.adapter.ItemsAdapter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * @author mojtaba hemmati sis
 */
@HiltViewModel
class FavoritesItemViewModel @Inject constructor(
    private val favRepository: FavoriteItemRepository
) : BaseViewModel<FavoritesItemListIntent>() {

    val dataState = MutableLiveData<FavoriteItemState>(FavoriteItemState.Idle)

    var adapter: ItemsAdapter = ItemsAdapter(object : ItemsAdapter.Listener {
        override fun requestChangeFavorite(position: Int, itemDto: ItemDto) {
            dataState.postValue(FavoriteItemState.RemoveFav(itemDto))
        }

    }).apply {
        addLoadStateListener { loadState -> uiState.postValue(analiseState(loadState, itemCount)) }
    }


    private suspend fun removeFavorite(position: Int, itemDto: ItemDto) {
        favRepository.removeFavorite(itemDto.toFavoriteData())
    }

    private suspend fun getItemList() {
        favRepository.getAll().collect {
            adapter.submitData(it)
        }
    }


    override fun onTriggerEvent(eventType: FavoritesItemListIntent) {

        viewModelScope.launch(Dispatchers.IO) {
            when (eventType) {
                is FavoritesItemListIntent.GetItemList -> getItemList()
                is FavoritesItemListIntent.RemoveFavorite -> removeFavorite(0, eventType.item)
            }
        }

    }

    override fun isFetchData(): Boolean = adapter.itemCount > 0

}