package com.sis.base.presention.base.adapter

sealed class AdapterState {
    object Loading : AdapterState()
    object Nothing : AdapterState()
    object Empty : AdapterState()
}