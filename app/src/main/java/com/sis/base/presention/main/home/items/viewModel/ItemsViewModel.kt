package com.sis.base.presention.main.home.items.viewModel

import androidx.lifecycle.viewModelScope
import com.sis.base.data.mapper.toFavoriteData
import com.sis.base.domain.model.ItemDto
import com.sis.base.domain.repository.ItemsRepository
import com.sis.base.presention.base.BaseViewModel
import com.sis.base.presention.main.home.items.adapter.ItemsAdapter
import com.sis.base.utils.ItemsStateAdapter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class ItemsViewModel @Inject constructor(
    private val itemRepository: ItemsRepository
) : BaseViewModel<ItemListIntent>() {

    private var _adapter: ItemsAdapter = ItemsAdapter(object : ItemsAdapter.Listener {
        override fun requestChangeFavorite(position: Int, itemDto: ItemDto) {
            onTriggerEvent(ItemListIntent.ChangeFav(position, itemDto))
        }

    }).apply {
        addLoadStateListener { loadState -> uiState.postValue(analiseState(loadState, itemCount)) }
    }

    private suspend fun changeFavorite(position: Int, itemDto: ItemDto) {
        val def = viewModelScope.async {
            itemRepository.changeFavoriteItem(itemDto.toFavoriteData(), itemDto.isFave)
        }
        def.await()
        withContext(Dispatchers.Main) {
            _adapter.customNotifyItemChanged(itemDto, position)
        }

    }

    val concatAdapter = _adapter.withLoadStateHeaderAndFooter(ItemsStateAdapter { retry() },
        ItemsStateAdapter { retry() })


    fun retry() {
        _adapter.retry()
    }

    fun getLastSearchText(): String? {
        return itemRepository.getLastSearchText()
    }

    private suspend fun addSearchText(searchText: String?) {
        itemRepository.addSearchText(searchText)
        withContext(Dispatchers.Main) {
            _adapter.refresh()
        }
    }

    private suspend fun getItemList() {
        itemRepository.getItemsWithFavorite().collectLatest {
            _adapter.submitData(it)
        }
    }

    override fun onTriggerEvent(eventType: ItemListIntent) {
        viewModelScope.launch(Dispatchers.IO) {
            when (eventType) {
                is ItemListIntent.AddSearchText -> addSearchText(eventType.searchText)
                is ItemListIntent.GetItemList -> getItemList()
                is ItemListIntent.ChangeFav -> changeFavorite(eventType.position, eventType.itemDto)
            }
        }
    }

    override fun isFetchData(): Boolean = _adapter.itemCount > 0

}