package com.sis.base.presention.main.home.items.adapter

import androidx.recyclerview.widget.DiffUtil
import com.sis.base.domain.model.ItemDto

class ItemsDiffCallBack : DiffUtil.ItemCallback<ItemDto>() {
    override fun areItemsTheSame(oldItem: ItemDto, newItem: ItemDto): Boolean {
        return oldItem.url == newItem.url
    }

    override fun areContentsTheSame(oldItem: ItemDto, newItem: ItemDto): Boolean {
        return oldItem.url == newItem.url
    }
}